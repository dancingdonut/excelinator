from collections import defaultdict
import json
from pprint import pp

import pyexcel as pyxl
from pyexcel.sheet import Sheet

SPREADSHEET_REL_PATH = 'spreadsheets/20210106_Part 14_Office.xlsx'
TYPES_SHEET_NAME = 'office_enum+enum(mf)'

book = pyxl.get_book_dict(file_name=SPREADSHEET_REL_PATH)
type_sheet: Sheet = pyxl.get_sheet(file_name=SPREADSHEET_REL_PATH,
                                   sheet_name=TYPES_SHEET_NAME)

enums = defaultdict(list)

for col in type_sheet.columns():
    if col[0] == 'Enum' or col[0] == 'Enum (mf)':
        for val in col[2:]:
            if isinstance(val, int):
                enums[col[1]].append(str(val))
            elif isinstance(val, str) and val.strip() != '':
                enums[col[1]].append(val.replace('\t', '').replace('\n', ''))

schemas = {}

def determine_min_max_length(enum):
    if len(enum) == 0:
        return 0, 0

    return len(min(enum, key=len)), len(max(enum, key=len))

def determine_type(name):
    return {
        'Text': 'string',
        'Integer': 'integer',
        'Enum': 'enum', # Hack to distinguish it temporarily from string.
        'Enum (mf)': 'array',
        'Boolean': 'boolean'
    }.get(name, 'string')

for entry in book['office'][1:]:
    key = entry[0].strip()
    title = entry[1].strip()
    schemas[key] = {
        'type': determine_type(entry[3]),
        'title': title,
        'description': 'tbd'
    }

    if schemas[key]['type'] == 'string':
        schemas[key]['minLength'] = 1
        schemas[key]['maxLength'] = 30

    if schemas[key]['type'] == 'enum':
        schemas[key]['enum'] = enums[title]
        schemas[key]['minLength'], schemas[key]['maxLength'] = determine_min_max_length(enums[title])
        schemas[key]['type'] = 'string'

    if schemas[key]['type'] == 'array':
        schemas[key]['minItems'] = 1
        schemas[key]['maxItems'] = 3
        schemas[key]['uniqueItems'] = True
        schemas[key]['items'] = {}
        schemas[key]['items']['type'] = 'string'
        schemas[key]['items']['pattern'] = '^[A-Za-z0-9 _.,äöüÄÖÜßéèáàêçñ-]+$'
        schemas[key]['items']['enum'] = enums[title]
        schemas[key]['items']['minLength'], schemas[key]['items']['maxLength'] = determine_min_max_length(enums[title])

    if schemas[key]['type'] == 'string':
        schemas[key]['pattern'] = '^[A-Za-z0-9 _.,äöüÄÖÜßéèáàêçñ-]+$'

DUMP_REL_PATH = 'json_dumps/office.json'

with open(DUMP_REL_PATH, 'w', encoding='utf8') as fp:
    json.dump(schemas, fp, indent=4, ensure_ascii=False)
